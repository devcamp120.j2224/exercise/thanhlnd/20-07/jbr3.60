package com.devcamp.s50.taskjbr360.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountControl {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
       Account account1 = new Account("01", "Giang", 10000); 
       Account account2 = new Account("02", "Ninh", 20000); 
       Account account3 = new Account("03", "Hải", 30000);
       Account account4 = new Account("04", "Thạnh", 40000);
       System.out.println(account1.toString()); 
       System.out.println(account2.toString()); 
       System.out.println(account3.toString());
       System.out.println(account4.toString());
       ArrayList<Account> accounts = new ArrayList<>();
       accounts.add(account1);
       accounts.add(account2);
       accounts.add(account3);
       accounts.add(account4);
       return accounts;
    }
}
